#  Update of https://www.raywenderlich.com/830-macos-nstableview-tutoria

````
/*
* Copyright (c) 2016 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
````
12 november 2020

I'm playing around with NSTableView and found that this subjects lacks recent coverage. The RayWenderling tutorial of the subject is
the closest to what I was looking for but was written for Swift 3 / Xcode 8.

This was the opportunity to revisit this piece step by step and figure out how it works.

The original article I used is here : https://www.raywenderlich.com/830-macos-nstableview-tutoria 

I only made the little adjustments here and there suggested by Xcode to make it compile and run on OS 10.14 / Xcode 11 / Swift 5, 
and made a commit at the end of each important step. Note that the commits lack full credit to original authors and some copyright 
notices may be missing. This is corected in this commit.

DISCLAIMER: Warren Burton and Ernesto Garcia deserve all the credit for this code. This taught me a lot and I thank them for it.

cedric.ponchy@gmail.com
